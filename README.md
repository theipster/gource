# gource

GitLab pipeline job configuration for generating [gource](https://gource.io) visualizations for a Git repository.

## Usage

```yaml
include:
  - remote: https://gitlab.com/theipster/gource/-/raw/main/gource.yml

gource:
  extends: .gource
```
